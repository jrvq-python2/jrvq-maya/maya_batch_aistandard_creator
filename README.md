# aiStandard batch generator for Maya

For every new shader, this tool asks the user to input a valid existing directory.
It will look inside the directory for the necessary files/textures to connect to the shader (as a PBR metal-rough-normal material). These texture files should follow the naming convention:

- *BaseColor\*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(i.e. Test_Material_BaseColor.png)
- *Metallic\*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(i.e. Test_Material_Metallic.png)
- *Roughness\*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(i.e. Test_Material_Roughness.png)
- *Normal\*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(i.e. Test_Material_Normal.png)

Under the advanced options of the tool, it is also possible to modify the regular expressions under which the textures are searched for.

For a quicker usage, it is possible to create multiple shaders at once, by selecting a folder which contains multiple subfolders with the conditions specified above.


### Recommended way to launch
Install the module to your desired location and run:
```
from maya_batch_aiStandard_creator import batch_aiStandard_UI
reload(batch_aiStandard_UI)
b = batch_aiStandard_UI.AiBatchCreator()
```

### Look of the widget
![batch_creator](http://www.jaimervq.com/wp-content/uploads/2020/01/batch_creator.jpg)

This widget has been tested successfully in **Maya 2018.**
***

For more info: www.jaimervq.com
