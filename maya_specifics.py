# -*- coding: UTF-8 -*-
'''
Author: Jaime Rivera
File: maya_specifics.py
Date: 2019.07.26
Revision: 2020.01.13
Copyright: Copyright 2019 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief:

'''

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2019, Jaime Rivera'
__credits__ = []
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Testing'


from maya import cmds
import pymel.core as pm


def get_selection():
    return cmds.ls(selection=True)

def create_ai_shader(shader_name, basecolor_path, metallic_path, roughness_path, normal_path, target_geo):
    
    # SG
    cmds.shadingNode('aiStandardSurface', asShader=True, name=shader_name)
    cmds.sets(name='{}_MaterialGroup'.format(shader_name), renderable=True, empty=True)
    cmds.connectAttr(shader_name + '.outColor', '{}_MaterialGroup.surfaceShader'.format(shader_name),
                     force=True)

    # Base color
    basecolor_file_node = cmds.shadingNode('file', name='{}_base_color'.format(shader_name), asTexture=True)
    cmds.setAttr('{}.fileTextureName'.format(basecolor_file_node), basecolor_path, type="string")
    cmds.connectAttr('{}.outColor'.format(basecolor_file_node), '{}.baseColor'.format(shader_name))

    # Metallic
    metallic_file_node = cmds.shadingNode('file', name='{}_metallic'.format(shader_name), asTexture=True)
    cmds.setAttr('{}.fileTextureName'.format(metallic_file_node), metallic_path, type="string")
    cmds.setAttr('{}.alphaIsLuminance'.format(metallic_file_node), True)
    cmds.setAttr('{}.colorSpace'.format(metallic_file_node), 'Raw', type='string')
    cmds.connectAttr('{}.outAlpha'.format(metallic_file_node), '{}.metalness'.format(shader_name))

    # Roughness
    roughness_file_node = cmds.shadingNode('file', name='{}_roughness'.format(shader_name), asTexture=True)
    cmds.setAttr('{}.fileTextureName'.format(roughness_file_node), roughness_path, type="string")
    cmds.setAttr('{}.alphaIsLuminance'.format(roughness_file_node), True)
    cmds.setAttr('{}.colorSpace'.format(roughness_file_node), 'Raw', type='string')
    cmds.connectAttr('{}.outAlpha'.format(roughness_file_node), '{}.specularRoughness'.format(shader_name))

    # Normal
    normal_file_node = cmds.shadingNode('file', name='{}_normal'.format(shader_name), asTexture=True)
    cmds.setAttr('{}.fileTextureName'.format(normal_file_node), normal_path, type="string")
    cmds.setAttr('{}.colorSpace'.format(normal_file_node), 'Raw', type='string')
    bump_2d_node = cmds.shadingNode('bump2d', name='{}_normal_bump2d'.format(shader_name), asTexture=True)
    cmds.setAttr('{}.bumpInterp'.format(bump_2d_node), 1)
    cmds.connectAttr('{}.outAlpha'.format(normal_file_node), '{}.bumpValue'.format(bump_2d_node))
    cmds.connectAttr('{}.outNormal'.format(bump_2d_node), '{}.normalCamera'.format(shader_name))

    # GEOMETRY ASSIGNATION
    for geo in target_geo:
        cmds.sets(geo, fe='{}_MaterialGroup'.format(shader_name))

def color_management_fix():
    colMgmtGlob = pm.PyNode('defaultColorMgtGlobals')
    for f in pm.ls(type='file'):
        colMgmtGlob.cmEnabled >> f.colorManagementEnabled
        colMgmtGlob.configFileEnabled >> f.colorManagementConfigFileEnabled
        colMgmtGlob.configFilePath >> f.colorManagementConfigFilePath
        colMgmtGlob.workingSpaceName >> f.workingSpace
